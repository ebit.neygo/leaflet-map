const mymap = L.map("mapid").setView([3.540589, 98.649582], 13);

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
  }
).addTo(mymap);

//function
const arr = [];
function markerClick(e) {
  const atng = this.getLatLng();
  let obj = arr.find((el) => el.lat === atng.lat && el.lng === atng.lng);
  let popup = e.target.getPopup();
  popup.setContent(`You Clicked Marker : ${arr.indexOf(obj) + 1}`);
}
function callMarker(x, y) {
  L.marker([x, y])
    .addTo(mymap)
    .bindPopup(`lat : ${x} <br> lng : ${y}`)
    .openPopup()
    .on("click", markerClick);
}
mymap.on("click", (e) => {
  callMarker(e.latlng.lat, e.latlng.lng);
  arr.push(e.latlng);
});
