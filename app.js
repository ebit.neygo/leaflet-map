const express = require("express");
const expressLayouts = require("express-ejs-layouts")
const path = require("path");
const mapRoutes = require("./routes/map_routes");
const app = express();


app.use(expressLayouts)
//Templating engine
app.set("views", path.join(__dirname, "views"));
app.set('layout','./layouts/layout')
app.set("view engine", "ejs");

// routes
app.use("/map", mapRoutes);

//using static files
app.use(express.static("public"));
app.use("/css", express.static(__dirname + "public/css"));
app.use("/img", express.static(__dirname + "public/img"));
app.use("/js", express.static(__dirname + "public/js"));

//urlEncoded to read body request
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
//Running server
if (process.env.NODE_ENV !== "test") {
  app.listen(5000, () => console.log("Server running on 5000"));
}
module.exports = app;
